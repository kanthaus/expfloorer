---
date: 2019-11-22
title: Interactive Kanthaus Floorplan
description: A floorplan of Kanthaus made in SVG with inkscape, made interactive with CSS and d3.js
initialAuthor: doug
---

View quickstart:
- clone/copy `interactiveFloorplan`
- `$ cd path/to/interactiveFloorplan`
- `$ npx http-server`
- → open link in browser

---

_of cybernetic dreams..._

This project was started to provide accurate, canonical floorplans of kanthaus. Until time of writing (2019-11-22) the only plans made using accurate measurements were done by Matthias in October 2018 using Sweethome3D: [file](https://cloud.kanthaus.online/s/PkjEKxBF5oA2NLY)

But why do just that when you could have more features! Like:
- enhanced accessability → browser viewable
- extensible & browser-compatibile → SVG
- floorplans made with standard FOSS SVG editor → Inkscape
- client-side interactivity → CSS (+ D3.js where necessary)

I can using this project to do things like:
- booking sleeping places
- resource/item location (interfacing with a barcode/RFID webapp! Not manually listing)
- lining sign files and info pages associated with respective rooms (perhaps a pop-up side panel?)
- ...

My initial plan was to simply use Inkscape to make the floorplans, load the resulting into an SVG and style with a hand-written internal/external stylesheet. Issue with this approach:
- Inkscape sets attributes to elements by default inline, making it only possible to change things (e.g. `fill`) with `!important` tags later.
- Unsetting attributes is possible in Inkscape, allowing them to be inherited by CSS later! However, the `initial` values for `fill` and `stroke` (i.e. when unset) is `black` and `none` respectively, making working in Inkscape practically impossible :(
- Using `Extensions → Merge Styles into CSS...` to group similar elements into general CSS classes to switch between `unset` and a nice color was horribly manual and breaky...

Cue [D3.js](https://d3js.org/). I looked around several forums and posted on Mastodon. Many people reckon D3.js is now the canonical way to manipulate SVGs in the browser. Now using classes and IDs in inkscape (via XML editor) you can later manipulate attributes. [This inkscape extension](https://github.com/monomon/inkscape-set-css-class) makes it possible to set classes on multiple elements at the same time, optionally removing the existing styling.

Measurement methodology
- using the laser measurer (check mode!) for longer distances and stick for shorter
- doorways measured using internal span and excluding cosmetic wood facing
- doors approximated with quarter circles same length as doorway, starting at opening side of doorway
- windowways measured using internal span and from internal wall to external wall
- windows approximated with quarter circles same length as windowway, starting at middle of windowway
- trying to be accurate within centimeters: checking level when measuring, taking a couple measurements on uneven surfaces, etc.
- walls approximated as 'negative space' afterwards
- decide to show lower-half level: k20-B, k20-0, k20-1 + k20-0#, k20-2 + k20-1#, k20-3 + k20-2#
