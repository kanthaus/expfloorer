const svgSource = "floorplan_measured.svg";
const numberOfFloors = 6;
const floorWidth = 2400;
const floorHeight = 1250;
const shrinkDivider = 12;
const defaultFloor = 2;

//jsDisabledDenotifier();
  
//async function jsDisabledDenotifier() {
  //var jsDisabledNotice = document.getElementById("jsDisabled");
  //jsDisabledNotice.parentNode.removeChild(jsDisabledNotice);
//};

splitFloors(svgSource, numberOfFloors, floorWidth, floorHeight, defaultFloor);

async function splitFloors(svgSource, numberOfFloors, floorWidth, floorHeight) {
  // Fetch floorplan SVG  
  const srcData = await d3.xml(svgSource);  
  var floorLooper = numberOfFloors;
  for(numberOfFloors; floorLooper > 0; floorLooper--) {
    // Clone the loaded floorplan SVG (const object change!)
    const dataClone = srcData.cloneNode(true);
    // Append cloned SVG to respective `viewFloorN` div    
    document.getElementById("viewFloor" + floorLooper)
      .append(dataClone.documentElement);
    // Manipulate SVG viewBox to show one floor per div
    d3.select("#viewFloor" + floorLooper + " > svg")
      .classed("shrink", true)
      .attr("preserveAspectRatio", "none")
      .attr("viewBox", "0 " + (floorHeight * (numberOfFloors - floorLooper)) + " " + floorWidth + " " + floorHeight);
    floorExpander();
    infoPaneller();
    grayBlocker();
  };
  initialExpander(defaultFloor);
  // Unset inline fills for CSS inheritance
  d3.selectAll(".room, .doorway, .step, .windowway, .background").style("fill", null);
  // Make things 'see-through' to interactivity (e.g. expose rooms)
  d3.selectAll("text, .msmt, .door").style("pointer-events", "none");
};

function floorExpander() {
  d3.selectAll("[id^='viewFloor'] > svg").on("click", function() {
    if (d3.select(this).attr("class").includes("shrink")) {
      d3.selectAll(".expand")
        .attr("height", floorHeight/shrinkDivider)
        .classed("expand", false)
        .classed("shrink", true);      
      d3.select(this)
        .attr("height", floorHeight)            
        .classed("shrink", false)           
        .classed("expand", true);
    };
  });
};

async function infoPaneller() {
  d3.selectAll(".room").on("click", async function() {
    var relpage = "exampleInfos/" + this.id + ".md";
    if (d3.select(this.parentNode.parentNode.parentNode).attr("class").includes("expand")) {
      try {
        infoPanelID.innerHTML = marked(await d3.text(relpage));
      } catch (error) {
        infoPanelID.innerHTML = "No info page about this room yet, sorry!";
      };
    };
  });
};

function initialExpander(defaultFloor) {
  d3.select("#viewFloor" + defaultFloor + "> svg")
    .attr("height", floorHeight)
    .classed("shrink", false)
    .classed("expand", true);
};

function grayBlocker() {
  d3.selectAll(".grayBlocked").on("click", function() {
    d3.selectAll(".grayBlocked")
      .style("opacity", "1");
  });
};


